package nl.richardberendsen.math;

public class DescriptiveStats {
    private DescriptiveStats() {}

    public static void main(String[] args) {
        long[] data = new long[]{2, 4, 8};
        double gm = geometricMean(data);
        System.out.println("Geometric mean of 2, 4 and 8: " + gm );
    }

    public static double geometricMean(long[] x) {
        int n = x.length;
        if (n == 0) {
            throw new IllegalArgumentException(
                    "geometric mean of empty array x ill-defined");
        }
        double GM_log = 0.0d;
        for (int i = 0; i < n; ++i) {
            if (x[i] == 0L) {
                return 0.0d;
            }  
            GM_log += Math.log(x[i]);
        }   
        return Math.exp(GM_log / n);
    }

    public static long min(long[] x) {
        int n = x.length;
        if (n == 0) {
            throw new IllegalArgumentException(
                    "min of empty array x ill-defined");
        }
        long _min = x[0];
        for (int i = 1; i < n; ++i) {
            if (x[i] < _min) {
                _min = x[i];
            }
        }
        return _min;
    }

    public static long max(long[] x) {
        int n = x.length;
        if (n == 0) {
            throw new IllegalArgumentException(
                    "max of empty array x ill-defined");
        }
        long _max = x[0];
        for (int i = 1; i < n; ++i) {
            if (x[i] > _max) {
                _max = x[i];
            }
        }
        return _max;
    }
}

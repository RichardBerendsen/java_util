package nl.richardberendsen.math;

import java.util.TreeMap;
import java.util.SortedMap;

public class Interval implements Comparable<Interval>{

    // since Random generates a number in [0, 1),
    // we work with intervals [a, b), and can be sure
    // to cover the entire range, and nothing but the range.

    public final double a; // inclusive
    public final double b; // exclusive

    public Interval(double a, double b) {
        if (b < a) {
            throw new IllegalArgumentException("Interval: a <= b must hold");
        }
        this.a = a; // inclusive
        this.b = b; // exclusive
    }

    @Override
    public boolean equals(Object otherObj) {
        if (this == otherObj) return true;

        //use instanceof instead of getClass here for two reasons
        //1. if need be, it can match any supertype, and not just one class;
        //2. it renders an explicit check for "that == null" redundant, since
        //it does the check for null already - "null instanceof [type]" always
        //returns false. (See Effective Java by Joshua Bloch.)
        if ( !(otherObj instanceof Interval) ) return false;
        //Alternative to the above line :
        //if ( other == null || other.getClass() != this.getClass() ) return false;

        //cast to native object is now safe
        Interval other = (Interval)otherObj;

        //now a proper field-by-field evaluation can be made
        return overlap(other);
    }

    private boolean overlap(Interval other) {
        return (other.b > a && b > other.a);
    }

    @Override
    public int compareTo(Interval other) {
        if (overlap(other)) return 0;
        if (b <= other.a) return -1;
        if (other.b <= other.a) return 1;
        assert(false); // we should not reach this point, ever.
        return 0;
    }
}

# Java util classes and functions

Stuff that I found missing in the standard Java libraries, and quicker to
implement myself than to look for third-party java packages.

Feel free to take a look around, use what you want, improve things, add, and so
on.
